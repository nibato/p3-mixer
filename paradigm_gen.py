#!/usr/bin/env python3
import os
import re

PARADIGM_PATTERN = re.compile(r"^(.*)(_m_lion|_allbreeds)\.png$", re.IGNORECASE)

class Paradigm():
    def __init__(self, name, suffix):
        self.name = name
        self.suffix = suffix

def main():
    paradigms = []

    for root, dirs, files in os.walk("paradigms"):
        for fname in files:
            fname = os.path.basename(fname)

            match = PARADIGM_PATTERN.match(fname)
            if not match:
                continue

            name, suffix = match.groups()

            paradigms.append(Paradigm(name, suffix))

    paradigms.sort(key=lambda p: p.name)

    with open("paradigms.js", "w") as f:

        f.write("var paradigms = [")

        first = True
        for p in paradigms:
            if not first:
                f.write(",")
            else:
                first = False

            f.write('\n\t\t[ "%s", "%s" ]' % (p.name, p.suffix))

        f.write("\n\t];")

if __name__ == "__main__":
    main()